#!/usr/bin/env python3
# ! -*- coding:utf-8 -*-
import random


# procedimiento que analiza quienes y cuantos aprueban y reprueban
def aprueba_o_reprueba(promedio):
    apruebo = 0
    repruebo = 0
    for i in range(31):
        if promedio[i][1] >= 4:
            print('Estudiante', i + 1, 'aprobó: ', round(promedio[i][1], 2))
            apruebo += 1
        else:
            print('Estudiante', i + 1, 'reprobó: ', round(promedio[i][1], 2))
            repruebo += 1
    print('Aprobaron', apruebo, 'y reprobaron', repruebo)


# imprime las notas y calcula promedio
def imprimir_notas_estudiantes(estudiantes, cantidad):
    print('Notas y promedios del módulo "Programación I"')
    for i in range(31):
        print('Estudiante', i + 1, ', notas: ', estudiantes[i][1])
    # lista con cada estudiante y su promedio
    promedio = []
    for i in range(31):
        promedio.append((i + 1, (sum(estudiantes[i][1]) / cantidad)))
    aprueba_o_reprueba(promedio)


# da las notas
def notas(cantidad):
    notas = []
    # se agregan notas
    for i in range(cantidad):
        notas.append(round(random.uniform(1, 7), 1))
    return notas


# crea la matriz con cada estudiante y sus notas
def crear_matriz(cantidad):
    estudiantes = []
    # se asigna a cada estudiante un número y sus notas, dadas por una función
    for i in range(31):
        estudiantes.append([i + 1, notas(cantidad)])
    return estudiantes


if __name__ == "__main__":
    # cantidad de notas aleatoria
    cantidad = random.randrange(4, 8)
    # crea matriz con 31 estudiantes y sus respectivas notas
    estudiantes = crear_matriz(cantidad)
    imprimir_notas_estudiantes(estudiantes, cantidad)
