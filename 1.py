#!/usr/bin/env python3
# ! -*- coding:utf-8 -*-


# de una lista vacía, retorna una de n elementos vacíos
def lista_vacia(largo):
    lista = []
    for i in range(largo):
        lista.append([])
    return lista


# crea lista
def crear_lista(tupla):
    # retorna una lista de n (largo tupla) elementos vacíos
    lista = lista_vacia(len(tupla))
    # lista[-1] = lista[n - 1], el último elemento es el primero de la tupla
    for i in range(len(tupla)):
        lista[i - 1] = tupla[i]
    print(tupla, lista)


if __name__ == "__main__":
    # tupla definida y enviada a procedimiento que crea lista
    tupla = ('hueso', 'ceniza', 'cristal', 'angeles', 'almas', 'fuego')
    crear_lista(tupla)
