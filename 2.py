#!/usr/bin/env python3
# ! -*- coding:utf-8 -*-
import random


# imprime la matriz
def imprimir_matriz(matriz):
    print('MATRIZ')
    for i in range(15):
        print(matriz[i])


# imprime la suma requerida
def imprimir_suma_de_filas(suma):
    print('Suma de las 5 primeras columnas')
    for i in range(15):
        print('Fila {0:^3}: {1:^3}'.format(i, suma[i]))
    # da la suma de las columnas en general
    suma_total = 0
    for i in range(15):
        suma_total += suma[i]
    print('La suma total de las 5 primeras columnas es: ', suma_total)


# imprime los datos requeridos
def imprimir_datos(matriz, menor, suma, negativos):
    imprimir_matriz(matriz)
    print('El menor de la matriz es: ', menor)
    imprimir_suma_de_filas(suma)
    print('Los números negativos en las columnas 5 a 9 son: ', negativos)


# calcula cuantos negativos hay en las columnas 5 a 9
def total_negativos(matriz):
    negativos = 0
    for i in range(15):
        # 4, 9 porque los indices comienzan del 0
        for j in range(4, 9):
            if matriz[i][j] < 0:
                negativos += 1
    return negativos


# recibe una fila y suma sus primeras 5 columnas
def suma_por_fila(fila):
    _suma = 0
    for i in range(5):
        _suma += fila[i]
    return _suma


# crea lista con las sumas de las 5 primas columnas de cada fila
def suma_5_columnas(matriz):
    suma = []
    for i in range(15):
        suma.append(suma_por_fila(matriz[i]))
    return suma


# recorre la matriz encontrando el número menor
def numero_menor(matriz):
    # se iguala a 11 porque es mayor que cualquier numero de la matriz
    menor = 11
    for i in range(15):
        for j in range(12):
            if menor > matriz[i][j]:
                menor = matriz[i][j]
    return menor


# procedimiento que entrega lo pedido
def operaciones(matriz):
    menor = numero_menor(matriz)
    """
        La suma de los elementos de las cinco primeras columnas de cada fila
        las cinco primeras columnas de cada fila son sus 5 primeros elementos
        por lo que se retornarían 12 valores con la suma correspondiente
    """
    suma = suma_5_columnas(matriz)
    negativos = total_negativos(matriz)
    imprimir_datos(matriz, menor, suma, negativos)


if __name__ == "__main__":
    matriz = []
    # crea matriz 15x12 con numeros entre -10 y 10
    for i in range(15):
        matriz.append([])
        for j in range(12):
            matriz[i].append(random.randint(-10, 10))
    operaciones(matriz)
